// ==UserScript==
// @name Lexpera++
// @version 0.1.11
// @description Adds some extra features to Lexpera.
// @description:tr Lexpera'ya ek özellikler katar.
// @namespace http://www.boran.me
// @author E. Ulaş Boran - ulas@boran.me
// @updateURL https://bitbucket.org/eubn/lexpera/raw/master/lexpera++.user.js
// @downloadURL https://bitbucket.org/eubn/lexpera/raw/master/lexpera++.user.js
// @match https://www.lexpera.com.tr/*
// @match https://lexpera.com.tr/*
// @grant GM_registerMenuCommand
// @run-at document-start
// @run-at context-menu
// ==/UserScript==

window.addEventListener("DOMContentLoaded", function(event) { 
  var lppMenu = document.createElement("nav");
  lppMenu.style.fontFamily = "arial";
  lppMenu.style.fontWeight = "bold";
  lppMenu.style.background = "orange";
  lppMenu.style.padding = "5px";
  lppMenu.style.width = "180px";
  lppMenu.style.zIndex = "100";
  lppMenu.style.position = "fixed";
  lppMenu.style.left = "0px";
  lppMenu.style.top = "0px";
  
  var lppMenuHeader = document.createElement("span");
  lppMenuHeader.style.fontSize = "0.9em";
  lppMenuHeader.style.display = "block";
  lppMenuHeader.style.margin = "0 0.5em";
  lppMenuHeader.style.color = "white";
  lppMenuHeader.innerHTML = "=== Lexpera++ ===";
  
  var buttonExtractSOPI = document.createElement("a");
  buttonExtractSOPI.style.fontSize = "0.9em";
  buttonExtractSOPI.style.display = "block";
  buttonExtractSOPI.style.margin = "0.5em 0.5em";
  buttonExtractSOPI.style.color = "white";
  buttonExtractSOPI.innerHTML = "SOPI Al";
  buttonExtractSOPI.addEventListener('click', getSOPI);
  
  var buttonExtractTitles = document.createElement("a");
  buttonExtractTitles.style.fontSize = "0.9em";
  buttonExtractTitles.style.display = "block";
  buttonExtractTitles.style.margin = "0.5em 0.5em";
  buttonExtractTitles.style.color = "white";
  buttonExtractTitles.innerHTML = "Başlık Al";
  buttonExtractTitles.addEventListener('click', getTitles);
  
  var buttonBottom = document.createElement("a");
  buttonBottom.style.fontSize = "0.9em";
  buttonBottom.style.display = "block";
  buttonBottom.style.margin = "0.5em 0.5em";
  buttonBottom.style.color = "white";
  buttonBottom.innerHTML = "v Sona Git";
  buttonBottom.addEventListener('click', goToBottom);
  
  var buttonTop = document.createElement("a");
  buttonTop.style.fontSize = "0.9em";
  buttonTop.style.display = "block";
  buttonTop.style.margin = "0.5em 0.5em";
  buttonTop.style.color = "white";
  buttonTop.innerHTML = "^ Başa Dön";
  buttonTop.addEventListener('click', goToTop);
  
  lppMenu.appendChild(lppMenuHeader);
  lppMenu.appendChild(buttonExtractSOPI);
  lppMenu.appendChild(buttonExtractTitles);
  lppMenu.appendChild(buttonTop);
  lppMenu.appendChild(buttonBottom);
  
  document.body.appendChild(lppMenu);
});

window.addEventListener("scroll", autoLoad, false);

function autoLoad () {
  moreButton = document.querySelectorAll('#ShowMoreSearchResults');
  if(moreButton.length > 0) {
    if (moreButton[0].style.display != 'none') {
      if((window.innerHeight + window.scrollY) >= document.body.scrollHeight) {
        document.querySelectorAll('#ShowMoreSearchResults > div.col-md-8 > div > a')[0].click();
      } 
    }
  }
}

function goToTop () {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}

function goToBottom () {
  document.body.scrollTop = document.body.scrollHeight;
  document.documentElement.scrollTop = document.body.scrollHeight;
}

function getSOPI () {
  var currentPATH = window.location.pathname;
  if (/^\/mevzuat\/arama/i.test(currentPATH)) {
    getSearchSOPI();
  } else if (/^\/ictihat\/arama/i.test(currentPATH)) {
    getSearchSOPI();
  } else if (/^\/literatur\/arama/i.test(currentPATH)) {
    getSearchSOPI();
  } else if (/^\/ornekler\/arama/i.test(currentPATH)) {
    getSearchSOPI();
  } else if (/^\/mevzuat-gunlugu/i.test(currentPATH)) {
    getMevzuatSOPI();
  }
}

function getTitles () {
  var currentPATH = window.location.pathname;
  if (/^\/mevzuat-gunlugu/i.test(currentPATH)) {
    getMevzuatTitles();
  } else if (/^\/mevzuat\/arama/i.test(currentPATH)) {
    getSearchTitles();
  } else if (/^\/ictihat\/arama/i.test(currentPATH)) {
    getSearchTitles();
  } else if (/^\/literatur\/arama/i.test(currentPATH)) {
    getSearchTitles();
  } else if (/^\/ornekler\/arama/i.test(currentPATH)) {
    getSearchTitles();
  }
}

function getMevzuatTitles () {
  var x=window.open();
  x.document.open();
  
  var links = document.querySelectorAll(".CalendarSmallAlert > a");

  Array.prototype.forEach.call(links, function (link) {
      x.document.write(link.innerHTML + "<br/>");
  });
  
  x.document.close();
}

function getSearchTitles () {
  var x=window.open();
  x.document.open();
  
  var links = document.querySelectorAll(".document-link");

  Array.prototype.forEach.call(links, function (link) {
      x.document.write(link.innerHTML + "<br/>");
  });
  
  x.document.close();
}

function getSearchSOPI () {
  var x=window.open();
  x.document.open();
  
  var re = /sopi\=\"(.*)\" id/g;
  var m;

  do {
      m = re.exec(document.body.innerHTML);
      if (m) {
          x.document.write(m[1] + "<br/>");
      }
  } while (m);
  
  x.document.close();
}

function getMevzuatSOPI () {
  var x=window.open();
  x.document.open();
  
  var re = /href\=\"\/mevzuat\/(?!Arama).*\/(.*)\"/g;
  var m;

  do {
      m = re.exec(document.body.innerHTML);
      if (m) {
          x.document.write(m[1] + "<br/>");
      }
  } while (m);
  
  x.document.close();
}